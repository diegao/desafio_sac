import logging
import pytest

from ..backend import db as _db
from ..backend import app as _app
from ..backend.config import config


@pytest.fixture(scope='session', autouse=True)
def app(request):
    ctx = _app.app_context()
    _app.config.from_object(config['testing'])
    ctx = _app.test_request_context()
    ctx.push()
    logging.disable(logging.CRITICAL)

    def teardown():
        ctx.pop()

    request.addfinalizer(teardown)
    return _app


@pytest.fixture(scope='session')
def test_client(app):
    return app.test_client()


@pytest.fixture(scope='session', autouse=True)
def build_db():
    _db.drop_all()
    _db.create_all()


@pytest.fixture(scope='session')
def db(request):
    return _db


@pytest.fixture(scope='function', autouse=True)
def rollback(app, db, request):
    def fin():
        _db.session.rollback()
    request.addfinalizer(fin)
