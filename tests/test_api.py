import json

from .factories import OperatorFactory, TicketFactory
from ..backend.models.operator import Operator
from ..backend.models.ticket import Ticket


class TestOperatorView:

    def teardown_method(self, method):
        Operator.query.delete()

    def test_operator_create(self, test_client, db):
        data = {
            'name': 'Test Operatror',
            'username': 'testoperator',
            'password': 123
        }
        response = test_client.post('/api/operators', data=data)
        rv = json.loads(response.data.decode('utf8'))

        assert response.status_code == 201
        assert 'Location' in response.headers
        assert rv['messages'] == 'Operator created'

    def test_operator_already_exists(self, test_client):
        data = {
            'name': 'Test Operatror',
            'username': 'testoperator',
            'password': 123
        }
        response = test_client.post('/api/operators', data=data)

        assert response.status_code == 201
        assert 'Location' in response.headers

        response = test_client.post('/api/operators', data=data)
        rv = json.loads(response.data.decode('utf8'))

        assert response.status_code == 202
        assert 'Location' in response.headers
        assert rv['messages'] == 'Operator already exists'

    def test_operators_list(self, test_client):
        OperatorFactory.create_batch(2)
        rv = test_client.get('/api/operators')
        data = json.loads(rv.data.decode('utf8'))

        assert rv.status_code == 200
        assert len(data) == 2

    def test_operator_get_by_username(self, test_client):
        OperatorFactory.create(username='test')

        response = test_client.get('/api/operators/test')
        rv = json.loads(response.data.decode('utf8'))

        assert response.status_code == 200
        assert rv['username'] == 'test'

    def test_list_operators_tickets(self, test_client):
        op = OperatorFactory.create()
        tickets = TicketFactory.create_batch(2, operator=op)
        op.tickets = tickets

        response = test_client.get('/api/operators/{}'.format(op.username))
        rv = json.loads(response.data.decode('utf8'))

        assert response.status_code == 200
        assert rv['username'] == op.username
        assert len(rv['tickets']) == 2
        assert 'tickets/{}'.format(tickets[0].id) in rv['tickets'][0]

    def test_operators_not_found(slef, test_client):
        response = test_client.get('/api/operators/someusername')
        rv = json.loads(response.data.decode('utf8'))

        assert response.status_code == 404
        assert rv['messages'] == 'Not found'

    def test_delete_operator(self, test_client):
        OperatorFactory.create(username='test')
        response = test_client.delete('/api/operators/test')
        rv = json.loads(response.data.decode('utf8'))

        assert response.status_code == 200
        assert rv['messages'] == 'Operator deleted'

    def test_delete_related_tickets_when_deletin_an_operator(self, test_client):
        op = OperatorFactory.create(username='test')
        ticket = TicketFactory.create(operator=op)
        response = test_client.delete('/api/operators/test')
        rv = json.loads(response.data.decode('utf8'))

        assert response.status_code == 200
        assert rv['messages'] == 'Operator deleted'

        response = test_client.delete('/api/tickets/{}'.format(ticket.id))
        rv = json.loads(response.data.decode('utf8'))

        assert response.status_code == 404


class TestTicketView:

    def setup_method(self, method):
        Operator.query.delete()
        Ticket.query.delete()

    def test_create_operators_ticket(self, test_client):
        op = OperatorFactory.create()
        data = {
            'state': 'RJ',
            'motive': 'Elogios',
            'category': 'Chat',
            'description': 'lorem ipsum dolor sit amet',
        }
        response = test_client.post('/api/tickets/{}'.format(op.username), data=data)
        rv = json.loads(response.data.decode('utf8'))

        assert response.status_code == 201
        assert 'Location' in response.headers
        assert rv['messages'] == 'Ticket created'

    def test_create_operator_ticket_with_empty_description(self, test_client):
        op = OperatorFactory.create()
        data = {
            'state': 'RJ',
            'motive': 'Elogios',
            'category': 'chat',
            'description': '',
        }
        response = test_client.post('/api/tickets/{}'.format(op.username), data=data)
        rv = json.loads(response.data.decode('utf8'))

        assert response.status_code == 422
        assert rv['messages']['description'][0] == 'Description can not be blank.'

    def test_update_ticket_with_empty_description(self, test_client):
        ticket = TicketFactory.create()
        data = {
            'state': 'RJ',
            'motive': 'Elogios',
            'category': 'Chat',
            'description': '',
        }
        response = test_client.put('/api/tickets/{}'.format(ticket.id), data=data)
        rv = json.loads(response.data.decode('utf8'))

        assert response.status_code == 422
        assert rv['messages']['description'][0] == 'Description can not be blank.'

    def test_create_operators_ticket_without_operator_created(self, test_client):
        data = {
            'state': 'RJ',
            'motive': 'Elogios',
            'category': 'Chat',
            'description': 'lorem ipsum dolor sit amet',
        }
        response = test_client.post('/api/tickets/unknownoperator', data=data)
        rv = json.loads(response.data.decode('utf8'))

        assert response.status_code == 404
        assert rv['messages'] == 'Operator unknownoperator does not exist.'

    def test_list_all_tickets(self, test_client):
        TicketFactory.create_batch(2)
        response = test_client.get('/api/tickets')
        rv = json.loads(response.data.decode('utf8'))

        assert response.status_code == 200
        assert len(rv) == 2

    def test_get_ticket_by_id(self, test_client):
        ticket = TicketFactory.create()
        response = test_client.get('/api/tickets/{}'.format(ticket.id))
        rv = json.loads(response.data.decode('utf8'))

        assert response.status_code == 200
        assert rv['id'] == ticket.id

    def test_tickets_not_fount(self, test_client):
        response = test_client.get('/api/tickets/1')
        rv = json.loads(response.data.decode('utf8'))

        assert response.status_code == 404
        assert rv['messages'] == 'Not found'

    def test_delete_ticket(self, test_client):
        ticket = TicketFactory.create()
        response = test_client.delete('/api/tickets/{}'.format(ticket.id))
        rv = json.loads(response.data.decode('utf8'))

        assert response.status_code == 200
        assert rv['messages'] == 'Ticket deleted'

    def test_update_ticket_data(self, test_client):
        ticket = TicketFactory.create()
        response = test_client.get('/api/tickets/{}'.format(ticket.id))
        rv = json.loads(response.data.decode('utf8'))

        assert response.status_code == 200
        assert rv['state'] == ticket.state

        data = {'state': 'MG'}
        response = test_client.put('/api/tickets/{}'.format(ticket.id), data=data)
        rv = json.loads(response.data.decode('utf8'))

        assert response.status_code == 200
        assert rv['messages'] == 'Ticket updated'

        response = test_client.get('/api/tickets/{}'.format(ticket.id))
        rv = json.loads(response.data.decode('utf8'))

        assert response.status_code == 200
        assert rv['state'] == 'MG'

    def test_list_tickets_from_operator(self, test_client):
        op = OperatorFactory.create()
        TicketFactory.create_batch(2, operator=op)
        response = test_client.get('/api/tickets/{}'.format(op.username))
        rv = json.loads(response.data.decode('utf8'))

        assert response.status_code == 200
        assert len(rv) == 2
        assert rv[0]['operator'] == op.id
        assert rv[1]['operator'] == op.id
