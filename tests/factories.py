import factory
from faker import Faker
from factory.alchemy import SQLAlchemyModelFactory

from ..backend.models.operator import Operator
from ..backend.models.ticket import Ticket
from .conftest import db

fake = Faker()


class OperatorFactory(SQLAlchemyModelFactory):

    class Meta:
        model = Operator
        sqlalchemy_session = db('').session

    id = factory.Sequence(lambda n: n + 1)
    name = factory.Faker('word')
    username = factory.Faker('word')
    password = factory.Faker('password')


class TicketFactory(SQLAlchemyModelFactory):

    class Meta:
        model = Ticket
        sqlalchemy_session = db('').session

    id = factory.Sequence(lambda n: n + 1)
    state = 'RJ'
    category = 'Telefone'
    motive = 'Dúvidas'
    description = fake.text(max_nb_chars=140)
    created_at = fake.date_time(tzinfo=None)
    updated_at = fake.date_time(tzinfo=None)
    operator = factory.SubFactory(OperatorFactory)
    operator_id = factory.LazyAttribute(lambda o: o.operator.id)
