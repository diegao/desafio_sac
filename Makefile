PYTHON=venv/bin/python3
PIP=venv/bin/pip
FLAKE8=venv/bin/flake8
FLASK=venv/bin/flask
GUNICORN=venv/bin/gunicorn
PYTEST=venv/bin/py.test

setup:
	virtualenv venv -p python3
	${PIP} install -U pip
	${PIP} install -r requirements-dev.txt
	@$(MAKE) initdb
	npm install
	npm run build-production

debug:
	FLASK_APP=backend/__init__.py FLASK_DEBUG=True ${FLASK} run

run:
	npm run build-production
	${GUNICORN} -b 0.0.0.0:5000 backend:app -w 4 --access-logfile=-

initdb:
	FLASK_APP=backend/__init__.py ${FLASK} initdb

pep8:
	${FLAKE8} --max-line-length=119 --exclude=venv/ . || true

test:
	${PYTEST} tests/ -s -vvv

clear-pyc:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +
