var webpack = require('webpack');

module.exports = function(env) {
    var config = {
        entry: './web/src/app.js',
        output: {
            path: './web/static/js',
            filename: 'bundle.js'
        },
        plugins : [
            new webpack.EnvironmentPlugin(['NODE_ENV', 'API_URL']),
        ],
        module: {
            loaders: [
                {
                    test: /\.js$/,
                    include: __dirname + '/web/src',
                    loader: 'babel',
                    query: {
                        presets: ['es2015', 'react'],
                        plugins: ['transform-object-rest-spread']
                    }
                }
            ],
        }
    }

    process.env.API_URL = process.env.API_URL || 'http://localhost:5000'
    if (env == 'production') {
        config.plugins.push(
            new webpack.optimize.UglifyJsPlugin({
                compress: {warnings: true}
            })
        )
    }
    else {
        config.devtool = 'source-map'
    }
    return config
}(process.env.NODE_ENV)
