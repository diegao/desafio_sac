# Desafio SAC

A implementação do deseafio foi dividida em duas partes, backend e frontend.

O *backend* do desafio foi implementado utilizando Python3, Flask, e o Gunicorn para servir a aplicação e os estáticos, entre outras dependências adicionadas. Foi desenvolvido no modelo de API REST para prover as informações e interações com o frontend.

O *frontend* do desafio foi implementado usando como tecnologias principais, o React, Redux, Webpack para gerenciar o build da aplicação assim como o NPM.
Para o caso deste desafio, o Gunicorn também fica encarregado de servir os estáticos, não sendo necessário levantar outro servidor.

# Setup do projeto

Dependencias para rodar o projeto: Python 3.4 e Node v5.8.0 instalados.

Clonar o projeto e entrar no diretório criado: `git clone https://diegao@bitbucket.org/diegao/desafio_sac.git && cd desafio_sac`

Executar no terminal `make setup`

# Executar o projeto

Executar no terminal `make run`

Criar registro de operador pela API do backend
```bash
curl -iXPOST http://localhost:5000/api/operators -d "name=teste&username=teste&password=123"
```

Acessar a página do operador no navegador [http://localhost:5000/teste](http://localhost:5000/teste).


# Rodar os testes da aplicação

`make test`

# PEP8
Checar se o código está de acordo com a PEP8: `make pep8`

# Interação com API

O Backend do projeto foi implementado como uma API REST, separada basicamente em dois recursos, um para manipular os operadores do SAC `/operatos` e outro para manipular os tickets `/tickets`.

### Tickets

`GET /api/tickets`

Listagem de todos os tickets cadastrados de todos os operadores.

`GET /api/tickets/<operator_username>`

Listagem de todos os tickets cadastrados pelo operador cujo username seja `<operator_username>`.

`GET /api/tickets/<id>`

Detalhamento do ticket pelo seu `<id>`.

`POST /api/tickets/<operator_username>`
```json
{
	"state": "RJ",
    "category": "Chat",
    "motive": "Elogios",
    "description": "Excelente atendimento"
}
```
Essa requisição com o payload acima, irá inserir um novo registro de ticket no banco de dados relacionado ao operador `<operator_username>`. Ela também retorna um header específico chamado `Location` com a localização do recurso recém criado `Location: http://localhost:5000/api/tickets/<id>`. Válido para demais operações `POST`.

Também se for feita a tentativa de inserir um registro duplicado, a API irá retornar o código `202` e também o header `Location` com o endereço para acessar o recurso existente. Válido para demais operações `POST`.


`PUT /api/tickets/<id>`
```json
{
	"state": "MG",
    "category": "Chat",
    "motive": "Dúvidas",
    "description": "Gostaria de receber mais  informações sobre o produto X"
}
```
Essa requisição irá atualizar o registro do ticket `<id>` com as informações acima informadas.

`DELETE /api/tickets/<id>`

Essa requisição irá remover do banco de dados o ticket `<id>`

### Operators

`GET /api/operators`

Listagem de todos os operadores cadastrados no banco de dados.

`GET /api/operatos/<username>`

Detalhes do operador `<username>`

`GET /api/operators/<username>/tickets`

Listagem dos tickets cadastrados no banco de dados para o operador `<username>`

`POST /api/operators`

```json
{
	"name": "Operador teste",
    "username": "teste",
    "password": 123
}
```
Essa requisição com o payload acima irá inserir um novo registro de operador no banco de dados. Também será retornado o header `Location` com o endereço para acessar o operador recém criado.

`DELETE /api/operators/<username>`

Remover registro do operador identificado pelo `<username>`. Também remove os tickets que estavam relacionados a este operador para evitar registros órfãos.
