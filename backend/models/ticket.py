from datetime import datetime
from sqlalchemy import Enum
from sqlalchemy.exc import IntegrityError

from .db import db
from .operator import Operator


StatesEnum = Enum(
    'MG', 'SP', 'RJ',
    name='states_enum'
)

CategoriesEnum = Enum(
    'Telefone', 'Chat', 'Email',
    name='categories_enum'
)

MotivesEnum = Enum(
    'Dúvidas', 'Elogios', 'Sugestões',
    name='motives_enum'
)


class Ticket(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    state = db.Column(StatesEnum, nullable=False)
    category = db.Column(CategoriesEnum, nullable=False)
    motive = db.Column(MotivesEnum, nullable=False)
    description = db.Column('description', db.Text, nullable=False)
    created_at = db.Column('created_at', db.DateTime, default=datetime.now())
    updated_at = db.Column('updated_at', db.DateTime, onupdate=datetime.now(), default=datetime.now())
    operator_id = db.Column('operator_id', db.Integer, db.ForeignKey(Operator.id))

    operator = db.relationship(Operator)

    def __repr__(self):
        return '<Ticket: {}-{}-{}>'.format(self.created_at, self.state, self.category)

    @classmethod
    def create(cls, data, operator_username):
        op = Operator.get_by_username(operator_username)
        if not op:
            return False, False

        data['operator_id'] = op.id
        ticket = Ticket(**data)
        db.session.add(ticket)

        try:
            db.session.commit()
        except IntegrityError as exc:
            print(exc)
            ticket = cls.query.filter(cls.id == data['id']).first()
            return True, ticket
        else:
            return False, ticket

    @classmethod
    def delete(cls, ticket):
        db.session.delete(ticket)
        db.session.commit()

    @classmethod
    def update(cls, ticket, data):
        [setattr(ticket, key, data[key]) for key in data.keys()]
        db.session.add(ticket)
        db.session.commit()

    @property
    def formatted_date(self):
        return datetime.strftime(self.created_at, '%d/%m/%Y')
