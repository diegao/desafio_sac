from sqlalchemy_utils import PasswordType
from sqlalchemy.exc import IntegrityError

from .db import db


class Operator(db.Model):
    __tablename__ = 'Operator'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    username = db.Column(db.String(50), unique=True, nullable=False)
    password = db.Column(PasswordType(schemes=['pbkdf2_sha512']), nullable=False)

    tickets = db.relationship('Ticket', cascade='delete')

    def __repr__(self):
        return '<Operator: {}>'.format(self.username)

    @classmethod
    def create(cls, data):
        op = cls(**data)
        db.session.add(op)
        try:
            db.session.commit()
        except IntegrityError as exc:
            print(exc)
            db.session.rollback()
            op = Operator.query.filter(cls.username == op.username).first()
            return True, op
        else:
            return False, op

    @classmethod
    def get_by_username(cls, username):
        return cls.query.filter(cls.username == username).first()

    @classmethod
    def delete(cls, op):
        db.session.delete(op)
        db.session.commit()
