from flask import jsonify, request, render_template, abort, url_for
from flask.views import MethodView

from webargs import fields, ValidationError
from webargs.flaskparser import use_args

from .models.operator import Operator
from .models.ticket import Ticket

from .schemas import (
    operator_schema,
    operators_schema,
    ticket_schema,
    tickets_schema
)


class OperatorView(MethodView):
    methods = ['POST', 'GET', 'DELETE']
    operator_args = {
        'name': fields.Str(required=True),
        'username': fields.Str(required=True),
        'password': fields.Str(required=True)
    }

    @use_args(operator_args)
    def post(self, args):
        exists, op = Operator.create(args)
        if exists:
            messages, code = 'Operator already exists', 202
        else:
            messages, code = 'Operator created', 201
        location = url_for('operators_detail', username=op.username)
        return jsonify({'messages': messages}), code, {'Location': location}

    def get(self, username=None, *args, **kwargs):
        if not username:
            ops = Operator.query.all()
            return operators_schema.jsonify(ops), 200

        op = Operator.get_by_username(username)
        if not op:
            return jsonify({'messages': 'Not found'}), 404

        if username and 'tickets' in request.endpoint:
            return tickets_schema.jsonify(op.tickets), 200

        return operator_schema.jsonify(op)

    def delete(self, username):
        op = Operator.get_by_username(username)
        if not op:
            return jsonify({'messages': 'Not found'}), 404
        Operator.delete(op)
        return jsonify({'messages': 'Operator deleted'}), 200


class TicketView(MethodView):
    methods = ['POST', 'GET', 'DELETE', 'PUT']

    def description_validate(value):
        if not value:
            raise ValidationError("Description can not be blank.")

    ticket_args = {
        'state': fields.Str(required=True),
        'category': fields.Str(required=True),
        'motive': fields.Str(required=True),
        'description': fields.Str(required=True, validate=description_validate)
    }

    ticket_update_args = {
        'state': fields.Str(),
        'category': fields.Str(),
        'motive': fields.Str(),
        'description': fields.Str(validate=description_validate),
        'operator_username': fields.Str()
    }

    @use_args(ticket_args)
    def post(self, args, operator_username):
        exists, ticket = Ticket.create(args, operator_username)
        if not exists and not ticket:
            messages, code = 'Operator {} does not exist.'.format(request.view_args['operator_username']), 404
            return jsonify({'messages': messages}), code
        if not exists and ticket:
            messages, code = 'Ticket created', 201
        location = url_for('ticket_detail', ticket_id=ticket.id)
        return jsonify({'messages': messages}), code, {'Location': location}

    def get(self, ticket_id=None, operator_username=None):
        if not ticket_id and not operator_username:
            tickets = Ticket.query.all()
            return tickets_schema.jsonify(tickets), 200

        if operator_username and not ticket_id:
            tickets = Ticket.query.filter(Ticket.operator.has(username=operator_username))
            return tickets_schema.jsonify(tickets), 200

        ticket = Ticket.query.filter(Ticket.id == ticket_id).first()
        if not ticket:
            return jsonify({'messages': 'Not found'}), 404
        return ticket_schema.jsonify(ticket)

    def delete(self, ticket_id):
        ticket = Ticket.query.get(ticket_id)
        if not ticket:
            return jsonify({'messages': 'Not found'}), 404
        Ticket.delete(ticket)
        return jsonify({'messages': 'Ticket deleted'}), 200

    @use_args(ticket_update_args)
    def put(self, args, ticket_id):
        ticket = Ticket.query.filter(Ticket.id == ticket_id).first()
        if not ticket:
            return jsonify({'messages': 'Not found'}), 404
        Ticket.update(ticket, args)
        return jsonify({'messages': 'Ticket updated'}), 200


class OperatorPage(MethodView):
    methods = ['GET']

    def get(self, operator):
        if not Operator.get_by_username(operator):
            abort(404)
        return render_template('index.html', operator=operator)
