import os
import click
from flask import Flask, render_template
from flask_marshmallow import Marshmallow

from .config import config
from .models.db import db
from .views import OperatorView, TicketView, OperatorPage
from .errors import handle_unprocessable_entity


def create_app(config_name):
    app = Flask(__name__,
                template_folder=config[config_name].STATIC_DIR,
                static_folder=config[config_name].STATIC_DIR)
    app.config.from_object(config[config_name])
    db.init_app(app)
    Marshmallow(app)
    register_urls(app)
    register_error_handlers(app)
    return app


def register_urls(app):
    app.add_url_rule('/<string:operator>', view_func=OperatorPage.as_view('operator_page'))

    app.add_url_rule('/api/operators', view_func=OperatorView.as_view('operators'))
    app.add_url_rule('/api/operators', view_func=OperatorView.as_view('operators_list'))
    app.add_url_rule('/api/operators/<string:username>', view_func=OperatorView.as_view('operators_detail'))
    app.add_url_rule('/api/operators/<string:username>/tickets', view_func=OperatorView.as_view('operators_tickets'))

    app.add_url_rule('/api/tickets', view_func=TicketView.as_view('tickets_list'))
    app.add_url_rule('/api/tickets/<string:operator_username>', view_func=TicketView.as_view('tickets'))
    app.add_url_rule('/api/tickets/<int:ticket_id>', view_func=TicketView.as_view('ticket_detail'))


def register_error_handlers(app):
    app.register_error_handler(422, handle_unprocessable_entity)


env = os.environ.get('ENVIRONMENT', 'default')
app = create_app(env)


@app.cli.command()
def initdb():
    click.echo('init database')
    db.drop_all()
    db.create_all()


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


if __name__ == '__main__':
    app.run()
