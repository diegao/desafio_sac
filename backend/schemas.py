from flask_marshmallow import Marshmallow

from .models.operator import Operator
from .models.ticket import Ticket

ma = Marshmallow()


class TicketSchema(ma.ModelSchema):
    class Meta:
        model = Ticket
        exclude = ('created_at', 'updated_at')

    formatted_date = ma.String()

ticket_schema = TicketSchema()
tickets_schema = TicketSchema(many=True)


class OperatorSchema(ma.ModelSchema):

    class Meta:
        model = Operator
        exclude = ('password',)
    tickets = ma.List(ma.HyperlinkRelated('ticket_detail', url_key='ticket_id', external=True))

operator_schema = OperatorSchema()
operators_schema = OperatorSchema(many=True)
