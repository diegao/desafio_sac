import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    STATIC_DIR = os.path.abspath('web/static')


class DevelopmentConfig(Config):
    DEBUG = True
    sqlite = 'sqlite:///' + os.path.join(basedir, 'db-dev.sqlite')
    SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URL') or sqlite
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class TestingConfig(Config):
    TESTING = True
    sqlite = 'sqlite:///' + os.path.join(basedir, 'db-test.sqlite')
    SQLALCHEMY_DATABASE_URI = os.environ.get('TEST_DATABASE_URL') or sqlite


class ProductionConfig(Config):
    sqlite = 'sqlite:///' + os.path.join(basedir, 'db-prod.sqlite')
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or sqlite


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig
}
