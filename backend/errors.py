from flask import jsonify


def handle_unprocessable_entity(err):
    data = getattr(err, 'data')
    if data:
        messages = data['exc'].messages
    else:
        messages = ['Invalid request']
    return jsonify({'messages': messages}), 422
