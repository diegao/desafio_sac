import React, {PropTypes, Component} from 'react'
import _ from 'lodash'

import store from './store'
import {createTicket, changeTicket, selectTicket, deleteTicket} from './actions'

export class TicketsList extends Component {
    handleClick(ticket) {
        store.dispatch(selectTicket(ticket))
    }

    handleDelete(ticket) {
        if (confirm("Tem certeza ?"))
            store.dispatch(deleteTicket(ticket))
    }

    groupTickets() {
        let groupedItems = _.groupBy(this.props.tickets, function(item) {
            return item.formatted_date + " - " + item.state
        })

        return _.map(groupedItems, (tickets, title) => {
            let titleSlug = title.replace(/\//g, '-').replace(/ /g, '')
            return (
                <div key={title} className="panel panel-default">
                    {this.renderGroupedTicketTitle(title, titleSlug)}
                    {this.renderTicketItem(tickets, titleSlug)}
                </div>
            )
        })
    }

    renderGroupedTicketTitle(title, titleSlug) {
        return (
            <div className="panel-heading" role="tab">
              <h4 className="panel-title">
                <a role="button" data-toggle="collapse" href={"#"+titleSlug} aria-expanded="true" aria-controls={titleSlug}>
                    {title}
                </a>
              </h4>
            </div>
        )
    }

    renderTicketItem(tickets, titleSlug) {
        return(
            <div id={titleSlug} className="panel-collapse collapse in" role="tabpanel">
                <div className="panel-body">
                {tickets.map(t =>
                    <div className="row" key={t.id} style={{borderBottom: "1px solid #EEE", padding: "5px 0px"}}>
                        <div className="col-sm-1">{t.id}</div>
                        <div className="col-sm-1">{t.state}</div>
                        <div className="col-sm-2">{t.category}</div>
                        <div className="col-sm-2">{t.motive}</div>
                        <div className="col-sm-5">{t.description}</div>
                        <div className="col-sm-1 pull-right">
                            <button onClick={this.handleClick.bind(this, t)}><i className="glyphicon glyphicon-edit"/></button>
                            <button onClick={this.handleDelete.bind(this, t)}><i className="glyphicon glyphicon-remove"/></button>
                        </div>
                    </div>
                )}
                </div>
            </div>
        )
    }

    render() {
        return (
            <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                {this.groupTickets()}
            </div>
        )
    }
}


export class TicketForm extends Component {
    constructor() {
        super()
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleSubmit(e) {
        e.preventDefault()
        store.dispatch(createTicket(this.props.ticket))
    }

    handleChange(e) {
        store.dispatch(changeTicket({[e.target.name]: e.target.value}))
    }

    setFormErrorClass() {
        if(this.props.messages.description)
            return "form-group has-error"
        return "form-group"
    }

    render() {
        const {state, category, motive, description} = this.props.ticket
        return(
            <div className="panel panel-primary">
                <div className="panel-heading" style={{marginBottom: "10px"}}>
                    <h3 className="panel-title">Cadastrar Ticket</h3>
                </div>
                <form onSubmit={this.handleSubmit} className="form-horizontal">
                    <div className="form-group">
                        <label htmlFor="state" className="col-sm-2 control-label">Estado</label>
                        <div className="col-sm-8">
                            <select onChange={this.handleChange} name="state" value={state} className="form-control">
                                {['RJ', 'SP', 'MG'].map(state =>
                                    <option key={state} value={state}>{state}</option>
                                )}
                            </select>
                        </div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="state" className="col-sm-2 control-label">Categoria</label>
                        <div className="col-sm-8">
                        <select onChange={this.handleChange} name="category" value={category} className="form-control">
                            {['Telefone', 'Chat', 'Email'].map(category =>
                                <option key={category} value={category}>{category}</option>
                            )}
                        </select>
                        </div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="state" className="col-sm-2 control-label">Motivo</label>
                        <div className="col-sm-8">
                        <select onChange={this.handleChange} name="motive" value={motive} className="form-control">
                            {['Dúvidas', 'Elogios', 'Sugestões'].map(motive =>
                                <option key={motive} value={motive}>{motive}</option>
                            )}
                        </select>
                        </div>
                    </div>
                    <div className={this.setFormErrorClass()}>
                        <label htmlFor="state" className="col-sm-2 control-label">Descrição</label>
                        <div className="col-sm-8">
                            <textarea type="text" onChange={this.handleChange} name="description" value={description} className="form-control"/>
                        <label className="control-label" htmlFor="inputError1">{this.props.messages.description}</label>
                        </div>
                    </div>

                    <div className="panel-footer">
                        <div className="form-group">
                            <div className="col-sm-offset-2 col-sm-10">
                                <button type="submit" className="btn btn-primary">Salvar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}
