import fetch from "isomorphic-fetch"

export const RECEIVE_TICKETS = 'RECEIVE_TICKETS'
export const SELECT_TICKET = 'SELECT_TICKET'
export const CHANGE_TICKET = 'CHANGE_TICKET'
export const RESET_TICKET = 'RESET_TICKET'
export const FORM_MESSAGE = 'FORM_MESSAGE'

let API_URL = process.env.API_URL

function getResponse(response) {
  if (response.status >= 200 && response.status < 300) {
    return response.json()
  } else {
    var error = new Error(response.statusText)
    error.response = response.json()
    throw error
  }
}

export function requestTickets() {
    return dispatch => {
        return fetch(API_URL + '/api/tickets/' + window.operator)
            .then(response => getResponse(response))
            .then(json => dispatch(receiveTickets(json)))
    }
}

function receiveTickets(tickets) {
    return {
        type: RECEIVE_TICKETS,
        tickets
    }
}

export function createTicket(ticket) {
    return dispatch => {
        let baseUrl = API_URL + '/api/tickets/';
        let url = ''
        let method = ''
        if (ticket.id) {
            url = baseUrl + ticket.id
            method = 'PUT'
        }
        else {
            url = baseUrl + window.operator
            method = 'POST'
        }
        return fetch(url, {
            method: method,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(ticket)
        })
        .then(response => getResponse(response))
        .then(json => {
            dispatch(requestTickets())
            dispatch(resetTickets())
            dispatch(updateFormMessages({}))
        })
        .catch(error => {
            error.response.then(json => {
                dispatch(updateFormMessages(json.messages))
            })
        })
    }
}

export function selectTicket(ticket) {
    return {
        type: SELECT_TICKET,
        ticket
    }
}

export function changeTicket(data) {
    return {
        type: CHANGE_TICKET,
        data
    }
}

export function resetTickets() {
    return {
        type: RESET_TICKET
    }
}

export function deleteTicket(ticket) {
    return dispatch => {
        var url = API_URL + '/api/tickets/' + ticket.id;
        return fetch(url, {
            method: 'DELETE',
        })
        .then(response => getResponse(response))
        .then(json => {
            dispatch(requestTickets())
            dispatch(updateFormMessages({}))
        })
    }
}

export function updateFormMessages(messages) {
    return {
        type: FORM_MESSAGE,
        messages
    }
}
