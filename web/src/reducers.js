import {
    RECEIVE_TICKETS,
    SELECT_TICKET,
    CHANGE_TICKET,
    RESET_TICKET,
    FORM_MESSAGE
} from './actions'


export function tickets(state = [], action) {
    switch (action.type) {
        case RECEIVE_TICKETS:
            return action.tickets
        default:
            return state
    }
}

var ticketInitialState = {
    state: "RJ",
    category: "Telefone",
    motive: "Dúvidas",
    description: ""
}

export function selectedTicket(state=ticketInitialState, action) {
    switch (action.type) {
        case SELECT_TICKET:
            return action.ticket
        case CHANGE_TICKET:
            return {...state, ...action.data}
        case RESET_TICKET:
            return ticketInitialState
        default:
            return state
    }
}

export function statusMessage(state={}, action) {
    switch (action.type) {
        case FORM_MESSAGE:
            return action.messages
        default:
            return state
    }
}
