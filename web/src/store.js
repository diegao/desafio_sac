import thunk from 'redux-thunk'
import {createStore, combineReducers, applyMiddleware} from 'redux'
import {tickets, selectedTicket, statusMessage} from './reducers'


const rootReducer = combineReducers({tickets, selectedTicket, statusMessage})
export default createStore(rootReducer, applyMiddleware(thunk))
