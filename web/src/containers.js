import React, {PropTypes, Component} from 'react'

import {TicketsList, TicketForm} from './components'
import {requestTickets} from './actions'
import store from './store'


export default class App extends Component {

    constructor() {
        super()
        this.state = store.getState()
        store.subscribe(this.handleChange.bind(this))
    }

    handleChange() {
        this.setState(store.getState())
    }

    componentWillMount() {
        store.dispatch(requestTickets())
    }

    render() {
        const {tickets, selectedTicket, statusMessage} = this.state
        return (
            <div className="tickets">
                <TicketForm ticket={selectedTicket} messages={statusMessage}/>
                <TicketsList tickets={tickets}/>
            </div>
        )
    }
}
